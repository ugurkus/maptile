# Change Log

Siber Panel üzerinde yapılan değişiklikleri CHANGELOG dosyası üzerinden takip edebilirsiniz.

# [1.0.8] - 26-07-2019

- Numeric input componentindeki bir hat afixlendi.
- Feedback düzenlendi.
- Sdiebar üzerindeki gereksiz butonlar kaldırıldı.
- Profil sayfası kaldırıldı.

# [1.0.7] - 05-07-2019

- Numeric input componenti eklendi.
- Loading componenti güncellendi.

# [1.0.6] - 03-07-2019

- ErrorBoundry ekranı güncellendi.(burdaki servisi kullanarak hatalrın kaydedilmesini sağlayabilirsiniz)
- DevExDatatabledaki bazı hatalar düzeltildi.

# [1.0.5] - 03-05-2019

- Form builder componenti eklendi.
- Yeni hata sayfası eklendi.
- Layout için mobil desteği.

# [1.0.4] - 08-04-2019

- Uygulamar arası geçişte menülerin değişmemesi ile ilgili hata giderildi.
- Yeni env dosyaları eklendi. polnet ve internet ortamında canlı ve test ortamları için build alabilmenizi sağlamaktadır.
- DevExpress datatable için grouping özelliği eklendi.
- Helper kütüphanesine yeni fonsiyonlar eklendi.
- Ant Design ve moment için proje genelinde Türkçe dil desteği sağlandı.

# [1.0.3] - 29-01-2019

- DevExpress Datatable;
- banded column,(detailrow özelliği ile birlikte kullanılmaz)
- tree data,
- inline editing,
- custom row component,
- fixed columns,
- column reordering,
- selecting row özelliğinde bazı değişiklikler var,(PropTypes kısmını inceleyerek değişiklikleri görebilirsiniz),
- prop tanımlamalarında iyileştirmeler

- AGGrid Datatable
- Excel benzeri yeni datatable.
- Prop değerleri component örnekleri ile birebir aynıdır.

- FusionCharts
- Projesinde chart kullanan arkadaşlar bu componenti kullanabilirler.
- FusionChart crack=>

- Adım 1=./node_modules/fusioncharts dizininde \.github\ değerini arayın.(3 farklı dosyada geçmekte)
  Adım 2=bulunan tüm dosyalara CREDIT_REGEX= değerinin hemen sonrasına projenizin çalıştığı url değerini ekleyin.
  örnek:CREDIT_REGEX=/120.100.4.26|120.100.217.155|fusioncharts\.github\.io\$/i... şeklinde olmalıdır.

- Diğer
- SSMConnect üzerinde kayıtlı yetkilendirilmiş menüleriniz oturum açma esnasında yüklenmektedir.
- Profil sayfası eklendi.(bu sayfayı kullanabilmek için servislerinin eklenmesi gerekmektedir.)
- babel-polyfill projeye eklendi

## [1.0.2] - 7-12-2018

- Props Validation kaldırıldı, eslint kuralları güncellendi.

## [1.0.1] - 28-11-2018

- Menü, SSM Connect yetkilendirme alt yapısına uyumlu hale getirildi.

## [1.0.0] - 22-11-2018

- CRA 2.0 alt yapısını kullanan kararlı sürüm yayınlandı.

/* eslint-disable no-param-reassign */
const CracoSwcPlugin = require('craco-swc');
const CracoAntDesignPlugin = require('craco-antd');

module.exports = {
  plugins: [
    {
      plugin: {
        ...CracoSwcPlugin,
        overrideCracoConfig: ({ cracoConfig }) => {
          if (typeof cracoConfig.eslint.enable !== 'undefined') {
            cracoConfig.disableEslint = !cracoConfig.eslint.enable;
          }
          delete cracoConfig.eslint;
          return cracoConfig;
        },
        overrideWebpackConfig: ({ webpackConfig, cracoConfig }) => {
          if (typeof cracoConfig.disableEslint !== 'undefined' && cracoConfig.disableEslint === true) {
            webpackConfig.plugins = webpackConfig.plugins.filter(
              (instance) => instance.constructor.name !== 'ESLintWebpackPlugin'
            );
          }
          return webpackConfig;
        },
      },
      options: {
        swcLoaderOptions: {
          jsc: {
            externalHelpers: true,
            target: 'es5',
            parser: {
              syntax: 'typescript',
              tsx: true,
              dynamicImport: true,
              exportDefaultFrom: true,
            },
          },
        },
      },
    },
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeTheme: {
          '@layout-header-background': '#2f4050',
          '@menu-dark-submenu-bg': '#293846',
          '@menu-dark-highlight-color': 'white',
          '@menu-dark-color': '#a7b1c2',
          '@menu-dark-item-active-bg': '#293846',
          '@menu-item-height': '32px',
          '@layout-body-background': '#f3f3f4',
          '@table-header-bg': '@background-color-light',
          '@table-header-sort-bg': '@background-color-base',
          '@table-row-hover-bg': '@primary-1',
          '@table-selected-row-bg': '#fafafa',
          '@table-padding-vertical': '8px',
          '@table-padding-horizontal': '8px',
          '@item-active-bg': '@primary-1',
          '@item-hover-bg': '@primary-1',
          '@primary-color': '#1DA57A',
        },
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};

// import 'react-app-polyfill/ie9';
// import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';
import { ConfigProvider } from 'antd';
/* eslint-disable-next-line */
import tr_TR from 'antd/lib/locale/tr_TR';
import moment from 'moment';
import 'moment/locale/tr';

import Routes from './config/Routes';

import * as serviceWorker from './serviceWorker';

moment.locale('tr');

/* eslint-disable react/jsx-filename-extension */
ReactDOM.render(
  /* eslint-disable-next-line */
  <ConfigProvider locale={tr_TR}>
    <Router>
      <Routes />
    </Router>
  </ConfigProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

export const queryString = (name, url = window.location.href) => {
  const regex = new RegExp(`[?&]${name.replace(/[[]]/g, '\\$&')}(=([^&#]*)|&|#|$)`, 'i');
  const results = regex.exec(url);
  if (!results || !results[2]) {
    return null;
  }
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

export const generateNonce = () =>
  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

export const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
};

/*eslint-disable */
if (!Array.prototype.first) {
  Array.prototype.first = function() {
    return this[0];
  };
}

if (!Array.prototype.last) {
  Array.prototype.last = function() {
    return this[this.length - 1];
  };
}

if (!Array.prototype.to1DArray ) {
  Array.prototype.to1DArray = function() {
    let newArr = [];

    for (let i = 0; i < this.length; i += 1) {
      newArr = newArr.concat(this[i]);
    }
    return newArr;
  };
}

if (!String.prototype.first) {
  String.prototype.first = function() {
    return this.charAt(0);
  };
}

if (!String.prototype.last) {
  String.prototype.last = function() {
    return this.charAt(this.length - 1);
  };
}

if (!String.prototype.isUpper) {
  String.prototype.isUpper = function() {
    return this.toUpperCase() === this;
  };
}

if (!String.prototype.isLower) {
  String.prototype.isLower = function() {
    return this.toLowerCase() === this;
  };
}

if (!String.prototype.urlParser) {
  String.prototype.urlParser = function() {
    const result = {};
    this.substr(this.indexOf('value'))
      .split('&')
      .forEach(item => {
        const param = item.split('=');
        result[param[0]] = decodeURIComponent(param[1]);
      });
    return result;
  };
}
/* eslint-enable */

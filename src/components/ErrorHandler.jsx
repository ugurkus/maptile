import openNotification from './Notification';

const logoutPrefix = process.env.NODE_ENV === 'development' ? '' : '/projenizinçalıştıgıyer';

const errorHandler = (error, code = null) => {
  if (typeof error === 'string') {
    openNotification('error', error);
  }

  if (code !== null) {
    switch (code) {
      case 'SC02':
        window.location = `${logoutPrefix}/#/logout`;
        break;
      case 'SC05':
        window.location = `${logoutPrefix}/#/logout`;
        break;
      case 'SC10':
        window.location = `${logoutPrefix}/#/logout`;
        break;
      case 'SC12':
        window.location = `${logoutPrefix}/#/logout`;
        break;
      default:
        console.error('HATA:', error); // eslint-disable-line
    }
  }
};

export default errorHandler;

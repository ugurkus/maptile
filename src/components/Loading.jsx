import React from 'react';
import { Layout, Row, Spin } from 'antd';

const Loading = (
  <Layout.Content>
    <Row type="flex" justify="space-around" align="middle" style={{ height: '100vh' }}>
      <Spin size="large" />
    </Row>
  </Layout.Content>
);

export const LoadingWithText = ({ tip }) => (
  <Row type="flex" justify="space-around" align="middle" style={{ height: '80vh' }}>
    <Spin tip={tip} size="large" />
  </Row>
);

export default Loading;

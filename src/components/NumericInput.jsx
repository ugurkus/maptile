import React from 'react';
import { Input } from 'antd';

export default class NumericInput extends React.Component {
  formatNumber = (value) => {
    value += ''; // eslint-disable-line
    const list = value.split('.');
    const prefix = list[0].charAt(0) === '-' ? '' : '';
    let num = prefix ? list[0].slice(1) : list[0];
    let result = '';
    while (num.length > 3) {
      result = `,${num.slice(-3)}${result}`;
      num = num.slice(0, num.length - 3);
    }
    if (num) {
      result = num + result;
    }
    return `${prefix}${result}${list[1] ? `.${list[1]}` : ''}`;
  };

  onChange = (e) => {
    const { value } = e.target;
    const reg = /^\d+$/;
    if ((!Number.isNaN(value) && reg.test(value)) || value === '') {
      this.props.onChange(value); // eslint-disable-line
    }
  };

  render() {
    return <Input {...this.props} onChange={this.onChange} />;
  }
}

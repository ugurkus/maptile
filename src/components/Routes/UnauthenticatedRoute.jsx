import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Layouts from '../../layouts';
import { appName } from '../../config';
import { queryString } from '../Helper';

const UnauthenticatedRoute = ({ component: C, props: cProps, ...rest }) => {
  const redirect = queryString('redirect');
  return (
    <Route
      {...rest}
      render={(props) =>
        sessionStorage.getItem(`${appName}_access_token`) === null ? (
          <Layouts type={rest.layout}>
            <C {...props} {...cProps} />
          </Layouts>
        ) : (
          <Redirect to={redirect === null ? '/' : redirect} />
        )
      }
    />
  );
};

export default UnauthenticatedRoute;

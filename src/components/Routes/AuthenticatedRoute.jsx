import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Layouts from '../../layouts/index';
import { appName } from '../../config';

const AuthenticatedRoute = ({ component: C, props: cProps, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      sessionStorage.getItem(`${appName}_access_token`) !== null ? (
        <Layouts type={rest.layout}>
          <C {...props} {...cProps} />
        </Layouts>
      ) : (
        <Redirect to={`/login?redirect=${props.location.pathname}${props.location.search}`} />
      )
    }
  />
);

export default AuthenticatedRoute;

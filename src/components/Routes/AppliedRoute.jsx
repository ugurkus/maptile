import React from 'react';
import { Route } from 'react-router-dom';

import Layouts from '../../layouts/index';

export default ({ component: C, props: cProps, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      <Layouts type={rest.layout}>
        <C {...props} {...cProps} />
      </Layouts>
    )}
  />
);

import { notification } from 'antd';

const openNotification = (type, description) => {
  notification[type]({ message: description });
};

export default openNotification;

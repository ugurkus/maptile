import React, { Component } from 'react';
import { Popover, Row, Button, Input, Col, Modal, Upload } from 'antd';
import axios from 'axios';

import { appName } from '../../config';
import './Feedback.scss';
import openNotification from '../../components/Notification';
import errorHandler from '../../components/ErrorHandler';

import { getBase64 } from '../../components/Helper';

const { TextArea } = Input;

export default class FeedBack extends Component {
  constructor() {
    super();
    this.state = {
      content: null,
      visible: false,

      previewVisible: false,
      previewImage: '',
      fileList: [],
    };
  }

  onVisibleChange = (visible) => {
    this.setState({
      visible,
    });
  };

  createFeedback = () => {
    const { content, fileList } = this.state;

    getBase64(fileList.first().originFileObj, (res) => {
      const CreateData = {
        info: content,
        error: fileList.length === 0 ? '' : res,
        clientId: process.env.REACT_APP_CLIENT_ID,
        userId: sessionStorage.getItem(`${appName}_user_id`),
        url: window.location.href,
        type: 'Bildirilen Hata',
      };
      axios({
        method: 'POST',
        // url: 'http://120.100.217.171/api/idpmgr/api/ErrorReport/CreateErrorReport',
        url: 'http://192.168.217.171/api/idpmgr/api/ErrorReport/CreateErrorReport',
        data: CreateData,
      })
        .then((response) => {
          const { isSuccess, resultMessage, errorCode } = response.data;
          if (isSuccess) {
            openNotification('success', 'Geri Bildirim İletildi.');
            this.setState({
              content: null,
              visible: false,
              fileList: [],
            });
          } else {
            errorHandler(resultMessage[0], errorCode);
          }
        })
        .catch((error) => {
          errorHandler(error.message, error.errorCode);
        });
    });
  };

  handleCancel = () => {
    this.setState({ previewVisible: false });
  };

  handleChange = ({ fileList }) => this.setState({ fileList });

  render() {
    const { content, visible } = this.state;
    const { previewVisible, previewImage, fileList } = this.state;
    const { type } = this.props;
    const uploadButton = (
      <div>
        <div className="ant-upload-text">Ekran Görüntüsü</div>
      </div>
    );
    return (
      <Popover
        content={
          type !== 'mobile' ? (
            <>
              <Row gutter={16}>
                <Col span={6}>
                  <div className="clearfix">
                    <Upload
                      accept="image/*"
                      listType="picture-card"
                      fileList={fileList}
                      onPreview={this.handlePreview}
                      onChange={this.handleChange}
                    >
                      {fileList.length >= 1 ? null : uploadButton}
                    </Upload>
                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                      <img alt="example" style={{ width: '100%' }} src={previewImage} />
                    </Modal>
                  </div>
                </Col>
                <Col span={18}>
                  <TextArea
                    style={{ height: '105px' }}
                    rows="4"
                    cols="60"
                    value={content}
                    placeholder="Karşılaştığınız hataları, görüş ve önerilerinizi geri bildirim formu aracılığıyla
                iletebilirsiniz."
                    onChange={(e) => {
                      this.setState({ content: e.target.value });
                    }}
                  />
                </Col>
              </Row>
              <Row gutter={16}>
                <Col span={24}>
                  <Button type="primary" style={{ float: 'right', marginTop: '16px' }} onClick={this.createFeedback}>
                    Gönder
                  </Button>
                </Col>
              </Row>
            </>
          ) : (
            <>
              <Row gutter={16}>
                <Row>
                  <TextArea
                    rows="8"
                    cols="25"
                    value={content}
                    placeholder="Karşılaştığınız hataları, görüş ve önerilerinizi geri bildirim formu aracılığıyla
              iletebilirsiniz."
                    onChange={(e) => {
                      this.setState({ content: e.target.value });
                    }}
                  />
                </Row>
                <Row>
                  <div className="uploadStyle">
                    <Upload
                      action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                      listType="picture-card"
                      fileList={fileList}
                      onPreview={this.handlePreview}
                      onChange={this.handleChange}
                    >
                      {fileList.length >= 1 ? null : uploadButton}
                    </Upload>
                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                      <img alt="example" style={{ width: '100%' }} src={previewImage} />
                    </Modal>
                  </div>
                </Row>
                <Row gutter={16}>
                  <Col span={24}>
                    <Button type="primary" style={{ float: 'right', marginTop: '16px' }} onClick={this.createFeedback}>
                      Gönder
                    </Button>
                  </Col>
                </Row>
              </Row>
            </>
          )
        }
        title="Hata Bildir"
        trigger="click"
        placement={type !== 'mobile' ? 'topRight' : 'topLeft'}
        visible={visible}
        onVisibleChange={this.onVisibleChange}
        className="Feedback"
      >
        <span className="Feedback-title">Hata Bildir</span>
      </Popover>
    );
  }
}

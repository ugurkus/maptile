import React, { Component } from 'react';
import { Col, Divider, Popover, Row } from 'antd';
import './ApplicationsMenu.scss';

export default class PopoverMenu extends Component {
  constructor() {
    super();
    this.state = {
      popoverVisible: false,
      menuData: [],
    };
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillMount = () => {
    const userApplications = sessionStorage.getItem('userApplications');
    if (userApplications !== null) {
      this.setState({ menuData: JSON.parse(userApplications) });
    }
  };

  onPopoverClick = (popoverVisible) => {
    this.setState({
      popoverVisible,
    });
  };

  render() {
    const { menuData, popoverVisible } = this.state;
    return (
      <Popover
        content={
          <Row gutter={16} className="ApplicationsMenu">
            {menuData.map((item, index) => (
              <React.Fragment key={item.id}>
                <a
                  style={{ padding: '0px', color: '#595959' }}
                  rel="noopener noreferrer"
                  target="_blank"
                  href={item.clientUri}
                >
                  <Col span={4}>
                    <span className="menu-name">{item.clientName}</span>
                  </Col>
                </a>
                {/* eslint-disable-next-line */}
                {++index % 3 === 0 && <Divider style={{ margin: '0px' }} />}
              </React.Fragment>
            ))}
          </Row>
        }
        title="SSMDB Uygulama Yönlendiricisi"
        trigger="click"
        placement="bottomRight"
        visible={popoverVisible}
        onVisibleChange={this.onPopoverClick}
        overlayClassName="ApplicationsMenu-overlay"
      />
    );
  }
}

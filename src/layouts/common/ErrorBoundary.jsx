import React from 'react';
import { Result, Row, Typography } from 'antd';
import PropTypes from 'prop-types';
import axios from 'axios';
import './ErrorBoundary.scss';

import { appName } from '../../config';

export default class ErrorBoundary extends React.Component {
  // eslint-disable-next-line react/static-property-placement
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    /* eslint-disable */
    axios({
      method: 'POST',
      url: 'http://120.100.217.171/api/idpmgr/api/ErrorReport/CreateErrorReport',
      // url: 'http://192.168.217.171/api/idpmgr/api/ErrorReport/CreateErrorReport',
      data: {
        error: error.toString(),
        info: info.componentStack.toString(),
        clientId: process.env.REACT_APP_CLIENT_ID,
        userId: sessionStorage.getItem(`${appName}_user_id`),
        url: window.location.href,
        type: 'Beklenmeyen Hata'
      }
    })
      .then(response => {
        console.log(response.data);
      })
      .catch(error2 => console.log(error2.message));

    console.group('errorMessages');
    console.log('error', error);
    console.log('info', info.componentStack.toString());
    console.groupEnd();
    /* eslint-enable */
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;

    return hasError ? (
      <Row type="flex" justify="space-around" align="middle" style={{ height: '100vh' }}>
        <Result
          extra={
            <a href="/">
              <Typography.Text type="bold">Anasayfaya dönmek için tıklayın...</Typography.Text>
            </a>
          }
          subTitle="Oluşan hata sistem tarafından kaydedilmiştir."
          title="Bir hata oluştu!"
          status="500"
        />
      </Row>
    ) : (
      children
    );
  }
}

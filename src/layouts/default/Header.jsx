/* eslint-disable no-unused-vars */
import React, { PureComponent } from 'react';
import { Button, Col, Dropdown, Layout, Menu, Row, Drawer, Typography, Divider } from 'antd';
import { Link } from 'react-router-dom';

import Feedback from '../common/Feedback';

import ApplicationsMenu from '../common/ApplicationsMenu';

import { appName } from '../../config';

import LayoutContext from './LayoutContext';

export default class Header extends PureComponent {
  // eslint-disable-next-line react/static-property-placement
  static contextType = LayoutContext;

  constructor() {
    super();
    this.state = {
      visible: false,
      content: true,
    };
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillMount = () => {
    const userApplications = sessionStorage.getItem('userApplications');
    if (userApplications !== null) {
      this.setState({ menuData: JSON.parse(userApplications) });
    }
  };

  openMobileMenu = () => {
    this.setState({
      visible: true,
    });
  };

  showApps = () => {
    const { content } = this.state;
    this.setState({
      content: !content,
    });
  };

  render() {
    const fullName = sessionStorage.getItem(`${appName}_full_name`);

    const { visible, content, menuData } = this.state;

    const {
      toggle,
      state: { collapsed, isMobile },
    } = this.context;
    const dropdownMenu = (
      <Menu>
        <Menu.Item key="1">
          <Link to="/logout" />
        </Menu.Item>
      </Menu>
    );

    return (
      <Layout.Header className="Header">
        {isMobile ? (
          <Row type="flex" justify="space-between">
            <Col />
            <Col style={{ marginRight: 16 }} />
          </Row>
        ) : (
          <Row type="flex" justify="space-between">
            <Col />
            <Col style={{ marginRight: 16 }}>
              <Feedback />
              <ApplicationsMenu />
              <Dropdown overlay={dropdownMenu}>
                <Button style={{ marginLeft: 8 }}>{fullName}</Button>
              </Dropdown>
            </Col>
          </Row>
        )}

        <Drawer
          width="50vw"
          title={fullName}
          placement="right"
          closable
          onClose={() =>
            this.setState({
              visible: false,
            })
          }
          visible={visible}
        >
          <Row onClick={this.showApps} style={{ cursor: 'pointer', textAlign: 'left', marginBottom: '10px' }}>
            <Typography.Text>Uygulamalar</Typography.Text>
          </Row>
          {content ? (
            <>
              <Row style={{ textAlign: 'left', marginBottom: '10px' }}>
                <Feedback type="mobile" />
              </Row>

              <Row style={{ textAlign: 'left', marginBottom: '10px' }}>
                <Link style={{ color: '#000000a6' }} to="/logout">
                  <Typography.Text>Çıkış</Typography.Text>
                </Link>
              </Row>
            </>
          ) : (
            <>
              <Divider />
              {menuData.map((item) => (
                <p key={item.id}>
                  <a
                    key={item.id}
                    style={{ padding: '0px', color: '#595959' }}
                    rel="noopener noreferrer"
                    target="_blank"
                    href={item.clientUri}
                  >
                    <Typography.Text>{item.clientName}</Typography.Text>
                  </a>
                </p>
              ))}
            </>
          )}
        </Drawer>
      </Layout.Header>
    );
  }
}

import React, { PureComponent } from 'react';

import { Col, Layout, Row } from 'antd';

import { NavLink } from 'react-router-dom';

import { appName } from '../../config';

import Menu from './Menu';

import LayoutContext from './LayoutContext';

class Sidebar extends PureComponent {
  // eslint-disable-next-line react/static-property-placement
  static contextType = LayoutContext;

  constructor(props) {
    super(props);

    this.state = {};
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillMount = () => {
    if (sessionStorage.getItem(`${appName}_full_name`) !== null) {
      this.setState({
        fullName: sessionStorage.getItem(`${appName}_full_name`),
      });
    }
  };

  render() {
    const {
      state: { collapsed },

      toggle,
    } = this.context;

    const { fullName } = this.state;

    return (
      <Layout.Sider
        theme="dark"
        breakpoint="lg"
        onBreakpoint={(e) => toggle(e)}
        collapsed={collapsed}
        className="Sidebar"
      >
        <Row gutter={16} type="flex" className="Sidebar-logo">
          <NavLink to="/" style={{ width: '100%' }}>
            <img src="./assets/img/logo_text.png" alt="Siber Panel" />
          </NavLink>
        </Row>

        <Row gutter={16} type="flex" className="Sidebar-logo-mini">
          <img src="./assets/img/logo_mini.png" alt="Siber Panel" />
        </Row>

        <Row gutter={16} type="flex" align="middle" className="Sidebar-user">
          <Col span={5} />

          <Col span={5} />

          <Col span={24} className="Sidebar-user-name">
            <h3>{fullName}</h3>
          </Col>
        </Row>

        <Menu time={Date.now()} />
      </Layout.Sider>
    );
  }
}

export default Sidebar;

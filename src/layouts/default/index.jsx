import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import Media from 'react-media';

import ErrorBoundary from '../common/ErrorBoundary';

import LayoutContext from './LayoutContext';
// import Sidebar from './Sidebar';
import './Layout.scss';

class DefaultLayout extends React.Component {
  // eslint-disable-next-line react/static-property-placement
  static propTypes = {
    content: PropTypes.instanceOf(Object).isRequired,
  };

  constructor() {
    super();
    this.state = {
      collapsed: false,
    };
  }

  render() {
    const { content } = this.props;
    const { collapsed } = this.state;

    return (
      <Media query="(max-width: 599px)">
        {(isMobile) => (
          <LayoutContext.Provider
            value={{
              state: {
                collapsed,
                isMobile,
              },
              toggle: (e) => {
                this.setState({
                  collapsed: e,
                });
              },
            }}
          >
            <ErrorBoundary>
              <Layout className="Container">
                {/* <Sidebar /> */}
                <Layout>{content}</Layout>
              </Layout>
            </ErrorBoundary>
          </LayoutContext.Provider>
        )}
      </Media>
    );
  }
}

export default DefaultLayout;

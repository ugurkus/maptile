import React from 'react';
import { Typography } from 'antd';

import './Layout.scss';

const FooterView = () => (
  <footer className="Footer">
    <Typography.Text type="secondary">Siber Suçlarla Mücadele Daire Başkanlığı</Typography.Text>
  </footer>
);
export default FooterView;

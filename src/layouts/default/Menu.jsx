import { Menu } from 'antd';
import axios from 'axios';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';

class Menus extends Component {
  // eslint-disable-next-line react/static-property-placement
  static propTypes = {
    location: PropTypes.instanceOf(Object).isRequired,
  };

  constructor() {
    super();
    this.state = {
      menus: [],
      openKeys: [],
    };
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillMount = () => {
    axios
      .get('menu.json')
      .then((res) => {
        this.setState({ menus: res.data.menu });
      })
      // eslint-disable-next-line no-console
      .catch((err) => console.log(err));
  };

  createSubMenu = (item) => (
    <Menu.SubMenu
      title={
        <span>
          <span>{item.name}</span>
        </span>
      }
      key={item.path}
    >
      {item.children &&
        item.children.map((child) =>
          child.children.length < 1 ? (
            <Menu.Item key={child.path}>
              <NavLink to={child.path}>{child.name}</NavLink>
            </Menu.Item>
          ) : (
            this.createSubMenu(child, child.path)
          )
        )}
    </Menu.SubMenu>
  );

  getDefaultOpenKeys = (data) => {
    const {
      location: { pathname },
    } = this.props;

    const getOpenKeys = data
      .filter((item) => item.children !== undefined)
      .filter((item) => item.children.filter((subMenu) => subMenu.path === pathname).length > 0);
    return getOpenKeys.length ? [getOpenKeys[0].path] : [];
  };

  render() {
    const {
      location: { pathname },
    } = this.props;
    const { menus, openKeys } = this.state;

    return (
      <Menu mode="inline" theme="dark" className="Menu" inlineIndent={20} selectedKeys={[pathname]} openKeys={openKeys}>
        {menus.map((menu) => (
          <Menu.Item key={menu.path}>
            <NavLink to={menu.path}>
              <span>{menu.name}</span>
            </NavLink>
          </Menu.Item>
        ))}
      </Menu>
    );
  }
}

export default withRouter(Menus);

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import DefaultLayout from './default';

const Layouts = (props) => {
  const { type, children } = props;
  const [theme, setTheme] = useState('light');
  switch (type) {
    case 'default':
      return <DefaultLayout content={children} theme={theme} setTheme={setTheme} />;
    default:
      return <>{children}</>;
  }
};

Layouts.propTypes = {
  type: PropTypes.string,
};

Layouts.defaultProps = {
  type: 'default',
};

export default Layouts;

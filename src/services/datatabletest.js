import axios from 'axios';

export const dataGenerator = (id) =>
  axios({
    method: 'GET',
    url: `http://192.168.4.88:5004/api/sampleanonymous/alltype/${id}`,
  })
    .then((response) => response)
    .catch((error) => Promise.reject(error));

export const xxx = (dataBody) =>
  axios({
    method: 'GET',
    url: `xxxx/${dataBody}`,
  })
    .then((response) => response)
    .catch((error) => Promise.reject(error));

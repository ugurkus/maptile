import { CodepenOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu, Checkbox } from 'antd';
import React from 'react';

import { layerShowHide } from './mapFunctions';

export default function LayerChooser(props) {
  const { map } = props;
  const sourceTypes = ['pts', 'baz'];

  return (
    <>
      <Dropdown
        overlay={
          <Menu>
            {sourceTypes.map((name) => (
              <Menu.Item key={name}>
                <Checkbox
                  onChange={(e) => {
                    layerShowHide(map, `${name}Points`, e.target.checked);
                    layerShowHide(map, `${name}Angle`, e.target.checked);
                  }}
                >
                  {name}
                </Checkbox>
              </Menu.Item>
            ))}
          </Menu>
        }
      >
        <Button>
          Data Layers <CodepenOutlined />
        </Button>
      </Dropdown>
    </>
  );
}

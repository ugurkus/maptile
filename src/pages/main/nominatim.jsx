import { Button, Input } from 'antd';
import axios from 'axios';
import React, { useState } from 'react';

import { addMarker, flyTo } from './mapFunctions';

let serviceCount;
let data;
// const apiUrl = 'http://192.168.1.241';
const apiUrl = 'http://192.168.1.6';
function arama(props) {
  const { map } = props;
  const [searchData, setSearchData] = useState([]);

  const sonuc = (newData) => {
    if (newData) {
      data = [...data, ...newData];
    }
    if (serviceCount === 0) {
      data = data.sort((a, b) => (a.place_rank < b.place_rank ? 1 : -1));
      setSearchData(data);
    }
  };

  const ara = (e) => {
    if (e.target.value === '' || e.target.value.length < 3) {
      setSearchData([]);
      return;
    }
    data = [];
    serviceCount = 6;
    axios({ method: 'GET', url: `${apiUrl}:8080/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8081/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8082/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8083/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8084/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8085/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8086/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8087/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
    axios({ method: 'GET', url: `${apiUrl}:8088/search.php?q=${e.target.value}` }).then((response) => {
      serviceCount -= 1;
      sonuc(response.data);
    });
  };

  const aramaSonucunaGit = (searchItem) => {
    // setSearchData();
    const coordinates = [searchItem.lon, searchItem.lat];
    addMarker(map, coordinates, { label: searchItem.display_name });
    flyTo(map, coordinates, 1);
  };

  return (
    <>
      <Input.Search onChange={ara} allowClear placeholder="nominatim-http://192.168.1.6" />
      {searchData.map((x) => (
        <Button type="dashed" onClick={() => aramaSonucunaGit(x)} className="buttonTextWarp">
          {x.display_name}
        </Button>
      ))}
    </>
  );
}

export default arama;

import { Button, Input } from 'antd';
import axios from 'axios';
import React, { useState } from 'react';

import { addMarker, flyTo } from './mapFunctions';

const apiUrl = 'http://192.168.1.242:55002';
function arama(props) {
  const { map } = props;
  const [searchData, setSearchData] = useState([]);

  const ara = (e) => {
    if (e.target.value === '') {
      setSearchData([]);
      return;
    }
    axios({ method: 'GET', url: `${apiUrl}/q/${e.target.value}` })
      .then((response) => {
        setSearchData(response.data.results);
      })
      .catch((error) => Promise.reject(error));
  };

  const aramaSonucunaGit = (searchItem) => {
    // setSearchData();
    const coordinates = [searchItem.lon, searchItem.lat];
    addMarker(map, coordinates, { label: searchItem.display_name });
    flyTo(map, coordinates, 1);
  };

  return (
    <>
      <Input.Search onChange={ara} allowClear placeholder="osmnames-'http://192.168.1.242:55002" />
      {searchData.map((x) => (
        <Button type="dashed" onClick={() => aramaSonucunaGit(x)} className="buttonTextWarp">
          {x.display_name} {x.state}
        </Button>
      ))}
    </>
  );
}

export default arama;

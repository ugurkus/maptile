import { CodepenOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from 'antd';
import React from 'react';

const mapUrl = process.env.REACT_APP_MAP;

export default function SytleChooser(props) {
  const { map, setStyleActive } = props;

  // const geoServerStyle = () => {
  //   axios({
  //     method: 'GET',
  //     url: 'http://192.168.1.242:8080/geoserver/rest/styles/btDaire.mbstyle',
  //     headers: { wj483hcf: 'admin' },
  //   }).then((response) => {
  //     setTest(response.data);
  //   });
  // };

  // useEffect(() => {
  //   geoServerStyle();
  // }, []);

  const styleNames = [
    { name: 'maptiler/streets', url: `${mapUrl}streets/style.json` },
    { name: 'maptiler/basic', url: `${mapUrl}basic/style.json` },
    { name: 'maptiler/bright', url: `${mapUrl}bright/style.json` },
    { name: 'maptiler/satellite-hybrid', url: `${mapUrl}satellite-hybrid/style.json` },
    { name: 'maptiler/tileservergl-streets', url: 'http://192.168.1.242:8088/styles/streets/style.json' },
    { name: 'maptiler/tileservergl-basic', url: 'http://192.168.1.242:8088/styles/basic/style.json' },
    { name: 'maptiler/tileservergl-bright', url: 'http://192.168.1.242:8088/styles/bright/style.json' },
    {
      name: 'maptiler/tileservergl-satellite-hybrid',
      url: 'http://192.168.1.242:8088/styles/satellite-hybrid/style.json',
    },
    { name: 'bt', url: 'http://192.168.1.242:8088/' },
    // {
    //   name: 'test',
    //   url: 'http://192.168.1.242:8080/geoserver/rest/styles/btDaire',
    // },
  ];

  const changeStyle = (item) => {
    if (item.name === 'bt') {
      map.current.setStyle({
        version: 8,
        sources: {
          btDaire: {
            type: 'vector',
            scheme: 'tms',
            tiles: [item.url],
            minzoom: 0,
            maxzoom: 14,
          },
        },
        glyphs: 'http://192.168.1.242:3650/api/fonts/{fontstack}/{range}.pbf',
        // sprite: 'http://192.168.1.242:3650/api/maps/streets/sprite',
        layers: [
          {
            id: 'Iller_region',
            source: 'btDaire',
            'source-layer': 'Iller_region',
            type: 'fill',
            paint: {
              'fill-color': 'rgba(210, 210, 195, 1)',
              'fill-opacity': 0.7,
            },
          },
          {
            id: 'IlMerkezi_font_point',
            source: 'btDaire',
            'source-layer': 'IlMerkezi_font_point',
            type: 'symbol',
            layout: {
              'text-field': '{ILADI} : {NUFUS}',
            },
            minzoom: 5,
          },
          {
            id: 'Ilceler_region',
            source: 'btDaire',
            'source-layer': 'Ilceler_region',
            type: 'fill',
            paint: {
              'fill-color': 'rgba(180, 210, 195, 1)',
              'fill-opacity': 0.7,
            },
            minzoom: 8,
          },
          {
            id: 'IlceMerkezi_font_point',
            source: 'btDaire',
            'source-layer': 'IlceMerkezi_font_point',
            type: 'symbol',
            layout: {
              'text-field': '{ILCEADI} : {NUFUS}',
            },
            minzoom: 8,
          },
          {
            id: 'Koymahalle_region',
            source: 'btDaire',
            'source-layer': 'Koymahalle_region',
            type: 'fill',
            paint: {
              'fill-color': 'rgba(155, 155, 195, 1)',
              'fill-opacity': 0.7,
            },
            minzoom: 11,
          },
          {
            id: 'YerlesimKoyMahalle_font_point',
            source: 'btDaire',
            'source-layer': 'YerlesimKoyMahalle_font_point',
            type: 'symbol',
            layout: {
              'text-field': '{ADI} : {NUFUS}',
            },
            minzoom: 11,
          },
        ],
      });
    } else {
      map.current.setStyle(item.url, {
        copySources: ['baz'],
        copyLayers: ['bazPoints', 'bazAngle'],
      });
    }
    setStyleActive(item.name);
  };

  return (
    <>
      <Dropdown
        overlay={
          <Menu>
            {styleNames.map((item) => (
              <Menu.Item key={item.name} icon={<CodepenOutlined />} onClick={() => changeStyle(item)}>
                {item.name}
              </Menu.Item>
            ))}
          </Menu>
        }
      >
        <Button>
          Stil Değiştir <CodepenOutlined />
        </Button>
      </Dropdown>
    </>
  );
}

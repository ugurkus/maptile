// eslint-disable-next-line
import maplibregl from '!maplibre-gl';
import { AutoComplete } from 'antd';
import axios from 'axios';
import React, { useState } from 'react';
import ReactDomServer from 'react-dom/server';
import moment from 'moment';

const url = process.env.REACT_APP_DIRECTIONS;
const apiUrl = 'http://192.168.1.242:55002';
// const apiUrl = 'http://192.168.1.242:55000';
const { Option } = AutoComplete;

function directions(props) {
  const { map } = props;
  const [wayPoints, setWayPoints] = useState([]);
  const [startSearchData, setStartSearchData] = useState([]);
  const [finishSearchData, setFinishSearchData] = useState([]);

  const clearMap = () => {
    if (map.current.getLayer('routes')) map.current.removeLayer('routes');
    if (map.current.getLayer('waypoint')) map.current.removeLayer('waypoint');
    if (map.current.getSource('waypoint')) map.current.removeSource('waypoint');
    if (map.current.getSource('waypoint')) map.current.removeSource('waypoint');
  };

  const getDirections = (wayPoints2) => {
    const waypointsString = wayPoints2
      .map((wp) => [wp.lon, wp.lat])
      .map((wp) => wp.join(','))
      .join(';');
    clearMap();
    axios({
      method: 'GET',
      url: `${url}${waypointsString}?overview=full&geometries=geojson&alternatives=true`,
    })
      .then((response) => {
        const { routes } = response.data;

        const wpGeojson = wayPoints2.map((wp) => ({
          type: 'Feature',
          properties: {
            name: wp.display_name,
            icon: 'car',
          },
          geometry: {
            type: 'Point',
            coordinates: [wp.lon, wp.lat],
          },
        }));

        routes.forEach((route, i) => {
          const geojson = {
            type: 'Feature',
            properties: {
              index: i,
              distance: route.distance,
              duration: route.duration,
              color: i === 0 ? '#888' : '#c7c7c7',
            },
            geometry: route.geometry,
          };
          wpGeojson.push(geojson);
        });

        map.current.addSource('waypoint', {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: wpGeojson,
          },
        });

        map.current.addLayer({
          id: 'routes',
          type: 'line',
          source: 'waypoint',
          filter: ['!=', '$type', 'Point'],
          layout: {
            'line-join': 'round',
            'line-cap': 'round',
          },
          paint: {
            'line-color': ['get', 'color'],
            'line-width': 4,
          },
        });
        map.current.addLayer({
          id: 'waypoint',
          type: 'symbol',
          source: 'waypoint',
          filter: ['==', '$type', 'Point'],
          layout: {
            'icon-image': 'car_15',
            'icon-overlap': 'always',
          },
        });

        // wayPoints2.forEach((wp, sıra) =>
        //   addMarker(map, wp, {
        //     label: `${sıra + 1} nolu nokta`,
        //     button: <Button>İşlem</Button>,
        //     onclick: () => {
        //       console.log('test');
        //     },
        //   })
        // );

        map.current.on('click', 'waypoint', (e) => {
          const { name } = e.features[0].properties;
          const popup = ReactDomServer.renderToString(<div>{name}</div>);
          new maplibregl.Popup().setLngLat(e.lngLat).setHTML(popup).addTo(map.current);
        });

        map.current.on('click', 'routes', (e) => {
          const { distance, duration } = e.features[0].properties;
          const popup = ReactDomServer.renderToString(
            <div>
              Mesafe : {(distance / 1000).toFixed(2)} km
              <br />
              Süre : {moment(new Date(duration * 1000)).format('HH:mm:ss')}
            </div>
          );
          new maplibregl.Popup().setLngLat(e.lngLat).setHTML(popup).addTo(map.current);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map.current.on('mouseenter', 'waypoint', () => {
          map.current.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map.current.on('mouseleave', 'waypoint', () => {
          map.current.getCanvas().style.cursor = '';
        });
      })
      .catch((error) => Promise.reject(error));
  };

  const startSearch = (e) => {
    if (e === '' || e.length < 3) {
      setStartSearchData([]);
      const tmp = [...wayPoints];
      tmp[0] = [];
      setWayPoints(tmp);
      return;
    }
    axios({ method: 'GET', url: `${apiUrl}/q/${e}` }).then((response) => {
      setStartSearchData(response.data.results);
    });
  };

  const finishSearch = (e) => {
    if (e === '' || e.length < 3) {
      setFinishSearchData([]);
      const tmp = [...wayPoints];
      tmp[1] = [];
      setWayPoints(tmp);
      return;
    }
    axios({ method: 'GET', url: `${apiUrl}/q/${e}` }).then((response) => {
      setFinishSearchData(response.data.results);
    });
  };

  const startSelect = (e, data) => {
    let tmp = [...wayPoints];
    if (tmp[0]) {
      tmp[0] = data;
    } else {
      tmp = [data];
    }
    setWayPoints(tmp);
    getDirections(tmp);
  };

  const finishSelect = (e, data) => {
    let tmp = [...wayPoints];
    if (!tmp[0]) {
      tmp = [[], data];
    } else if (tmp[1]) {
      tmp[1] = data;
    } else {
      tmp.push(data);
    }
    setWayPoints(tmp);
    getDirections(tmp);
  };

  return (
    <div>
      <AutoComplete
        style={{ width: '100%' }}
        onSearch={startSearch}
        onSelect={startSelect}
        placeholder="Başlangıç"
        allowClear
      >
        {startSearchData.map((data) => (
          <Option key={data.lon} value={data.display_name} {...data}>
            {data.display_name}
          </Option>
        ))}
      </AutoComplete>
      <AutoComplete
        style={{ width: '100%' }}
        onSearch={finishSearch}
        onSelect={finishSelect}
        placeholder="Bitiş"
        allowClear
      >
        {finishSearchData.map((data) => (
          <Option key={data.lon} value={data.display_name} {...data}>
            {data.display_name}
          </Option>
        ))}
      </AutoComplete>
    </div>
  );
}

export default directions;

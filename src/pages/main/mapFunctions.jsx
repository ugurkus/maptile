// eslint-disable-next-line
import maplibregl from '!maplibre-gl';
import { DeleteTwoTone } from '@ant-design/icons';
import React from 'react';
import ReactDomServer from 'react-dom/server';

export const addMarker = (map, LngLat, props = {}) => {
  const { draggable, label, button, onclick } = props;
  const marker = new maplibregl.Marker({ draggable }).setLngLat(LngLat);

  if (label) {
    const div = document.createElement('div');
    const remove = document.createElement('div');
    div.appendChild(remove);
    remove.innerHTML = ReactDomServer.renderToString(<DeleteTwoTone className="deleteMarker" />);
    remove.onclick = () => {
      marker.remove();
    };

    const labelDiv = document.createElement('p');
    div.appendChild(labelDiv);
    labelDiv.innerHTML = label && label;
    const buttonDiv = document.createElement('div');
    div.appendChild(buttonDiv);
    buttonDiv.innerHTML = ReactDomServer.renderToString(button);
    // const addpoint = () => {
    //   axios({
    //     method: 'POST',
    //     url: `http://localhost:8080/api/harita/ptsEkle`,
    //     data: LngLat,
    //   })
    //     .then((response) => response)
    //     .catch((error) => Promise.reject(error));
    // };
    buttonDiv.onclick = onclick;

    marker.setPopup(new maplibregl.Popup().setLngLat(LngLat).setDOMContent(div));
  }
  marker.addTo(map.current);
};

export const flyTo = (map, center, noAnimate) => {
  map.current.flyTo({
    center,
    zoom: 14,
    essential: !noAnimate, // this animation is considered essential with respect to prefers-reduced-motion
  });
};

export const layerShowHide = (map, name, state) => {
  if (state === true) {
    map.current.setLayoutProperty(name, 'visibility', 'visible');
  } else {
    map.current.setLayoutProperty(name, 'visibility', 'none');
  }
};

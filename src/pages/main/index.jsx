// eslint-disable-next-line
import maplibregl from '!maplibre-gl';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import { DPI, Format, MaplibreExportControl, PageOrientation, Size } from '@watergis/maplibre-gl-export';
import '@watergis/maplibre-gl-export/css/styles.css';
import { Button } from 'antd';
import { CircleMode, DirectMode, DragCircleMode, SimpleSelectMode } from 'mapbox-gl-draw-circle';
import DrawRectangle from 'mapbox-gl-draw-rectangle-mode';
import 'maplibre-gl/dist/maplibre-gl.css';
import React, { useEffect, useRef, useState } from 'react';

import Directions from './directions';
import LayerChooser from './LayerChooser';
import { addMarker } from './mapFunctions';
import NominatimArama from './nominatim';
import OsmnamesArama from './osmnames';
import './styles.css';
import SytleChooser from './SytleChooser';

const apiUrl = process.env.REACT_APP_API_URL;
const mapUrl = process.env.REACT_APP_MAP;

const sourceTypes = ['baz'];

export default function App() {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(32.83829);
  const [lat, setLat] = useState(39.93384);
  const [zoom, setZoom] = useState(6);
  const [pointFeatures, setPointFeatures] = useState([]);
  const [styleActive, setStyleActive] = useState('streets');

  const loadControls = () => {
    map.current.addControl(new maplibregl.NavigationControl());
    map.current.addControl(new maplibregl.FullscreenControl({ container: document.querySelector('body') }));
    map.current.addControl(
      new maplibregl.ScaleControl({
        maxWidth: 80,
        unit: 'metric',
      })
    );
    map.current.addControl(
      new MaplibreExportControl({
        PageSize: Size.A4,
        PageOrientation: PageOrientation.Portrait,
        Format: Format.PNG,
        DPI: DPI[300],
        Crosshair: true,
        PrintableArea: true,
      }),
      'top-right'
    );

    const draw = new MapboxDraw({
      // defaultMode: 'draw_circle',
      // userProperties: true,
      modes: {
        ...MapboxDraw.modes,
        draw_rectangle: DrawRectangle,
        draw_circle: CircleMode,
        drag_circle: DragCircleMode,
        direct_select: DirectMode,
        simple_select: SimpleSelectMode,
      },
      displayControlsDefault: false,
      // Select which mapbox-gl-draw control buttons to add to the map.
      controls: {
        polygon: true,
        trash: true,
      },
    });
    map.current.addControl(draw);

    const updateArea = () => {
      // console.log(e.features[0].geometry.coordinates);
      // const data = draw.getAll();
      // const answer = document.getElementById('calculated-area');
      // if (data.features.length > 0) {
      //   const area = turf.area(data);
      //   // Restrict the area to 2 decimal points.
      //   const roundedArea = Math.round(area * 100) / 100;
      //   answer.innerHTML = `<p><strong>${roundedArea}</strong></p><p>square meters</p>`;
      // } else {
      //   answer.innerHTML = '';
      //   if (e.type !== 'draw.delete') alert('Click the map to draw a polygon.');
      // }
    };

    map.current.on('draw.create', updateArea);
    map.current.on('draw.delete', updateArea);
    map.current.on('draw.update', updateArea);
  };
  const load3Dlayers = () => {
    // Insert the layer beneath any symbol layer.
    const { layers } = map.current.getStyle();

    let labelLayerId;
    for (let i = 0; i < layers.length; i += 1) {
      if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
        labelLayerId = layers[i].id;
        break;
      }
    }
    map.current.addLayer(
      {
        id: '3d-buildings',
        source: 'openmaptiles',
        'source-layer': 'building',
        filter: ['==', 'extrude', 'true'],
        type: 'fill-extrusion',
        minzoom: 15,
        paint: {
          'fill-extrusion-color': '#aaa',

          // use an 'interpolate' expression to add a smooth transition effect to the
          // buildings as the user zooms in
          'fill-extrusion-height': ['interpolate', ['linear'], ['zoom'], 15, 0, 15.05, ['get', 'height']],
          'fill-extrusion-base': ['interpolate', ['linear'], ['zoom'], 15, 0, 15.05, ['get', 'min_height']],
          'fill-extrusion-opacity': 0.6,
        },
      },
      labelLayerId
    );
  };

  // eslint-disable-next-line
  const loadExtraLayers = () => {
    sourceTypes.forEach((type) => {
      map.current.loadImage(`assets/img/${type}.png`, (error, image) => {
        if (error) throw error;
        map.current.addImage(`${type}Icon`, image);
      });

      map.current.addSource(type, {
        type: 'geojson',
        data: `${apiUrl}${type}.geojson`,
      });

      map.current.addLayer({
        id: `${type}Points`,
        type: 'symbol',
        source: type,
        filter: ['==', '$type', 'Point'],
        layout: {
          'text-field': '{name}',
          'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
          'text-size': 10,
          'text-offset': [0, 2.5],
          'icon-image': `${type}Icon`,
          'icon-size': 0.2,
          visibility: 'visible',
        },
        // minzoom: 10,
      });

      if (type === 'baz') {
        // circle and symbol layers for rendering individual earthquakes (unclustered points)
        map.current.addLayer({
          id: `${type}Angle`,
          type: 'fill',
          source: type,
          layout: {
            visibility: 'visible',
          },
          paint: {
            'fill-color': '#088',
            'fill-opacity': 0.2,
          },
          filter: ['==', '$type', 'Polygon'],
        });
      }
    });
  };

  useEffect(() => {
    if (map.current) return; // initialize map only once
    // harita oluşturma
    map.current = new maplibregl.Map({
      container: mapContainer.current,
      style: `${mapUrl}${styleActive}/style.json`,
      center: [lng, lat],
      zoom,
      trackResize: false,
    });
    loadControls();

    map.current.on('move', () => {
      setLng(map.current.getCenter().lng.toFixed(4));
      setLat(map.current.getCenter().lat.toFixed(4));
      setZoom(map.current.getZoom());
    });

    map.current.on('click', (e) => {
      if (e.originalEvent.ctrlKey === true) {
        addMarker(map, e.lngLat, { draggable: true, label: e.lngLat });
      }
      if (e.originalEvent.altKey === true) {
        const features = map.current.queryRenderedFeatures(e.point);
        const displayFeat = [];
        features.forEach((feat) => {
          displayFeat.push(feat);
        });
        setPointFeatures(
          <>
            <Button onClick={() => setPointFeatures()} style={{ position: 'sticky', top: 0 }}>
              Kapat
            </Button>
            <pre>{JSON.stringify(displayFeat, null, 2)}</pre>
          </>
        );
      }
    });

    map.current.on('idle', () => {
      map.current.resize();
      // const mapData = map.current.getStyle();
      // console.log('mapData', mapData);
    });

    // map.current.on('load', () => {
    //   const style = map.current.getStyle();
    //   console.log(style);
    // });

    map.current.on('style.load', () => {
      if (styleActive === 'streets') {
        load3Dlayers();
      }
      // const style = map.current.getStyle();
      // console.log(style);
      // loadExtraLayers();
    });
  });

  return (
    <div style={{ position: 'relative' }}>
      <div className="search">
        <Directions
          map={map}
          waypoints={[
            [32.86004197499295, 39.9359301618249],
            [32.868315766303084, 39.93974989177201],
          ]}
        />
        <NominatimArama map={map} />
        <OsmnamesArama map={map} />
      </div>
      <div className="styleChooser">
        <SytleChooser map={map} setStyleActive={setStyleActive} />
      </div>
      <div className="layerChooser">
        <LayerChooser map={map} />
      </div>
      <div id="features" className="features">
        {pointFeatures}
      </div>
      <div className="mapInfo">
        Lng: {lng} | Lat: {lat} | Zoom: {zoom}
      </div>
      <div ref={mapContainer} className="map-container" />
    </div>
  );
}

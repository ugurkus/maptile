import React from 'react';
import { Col, Layout, Row, Typography } from 'antd';

const { Content } = Layout;
const { Text } = Typography;

const NotFound = () => (
  <Content className="Content">
    <Row style={{ marginTop: '10vh', textAlign: 'center' }}>
      <Col xs={24} sm={24} md={12} lg={12} xl={12}>
        <img style={{ margin: 'auto', width: '60%' }} alt="404" src="./assets/img/404.svg" />
      </Col>
      <Row style={{ top: '15vh', textAlign: 'left' }}>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <Text style={{ fontSize: '60px' }} strong>
            404
          </Text>
          <br />
          <Text style={{ fontSize: '20px' }} type="secondary">
            Aradığınız Sayfa Bulunamadı!
          </Text>
          <br />
          <br />
          <a href="/">
            <Text type="secondary">Anasayfaya dönmek için tıklayın...</Text>
          </a>
        </Col>
      </Row>
    </Row>

    <table style={{ width: '100vw', marginTop: '10vh', marginLeft: '10vw' }}>
      <tr>
        <td style={{ width: '33%' }} />

        <td />
      </tr>
    </table>
  </Content>
);

export default NotFound;

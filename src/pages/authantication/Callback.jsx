/* eslint-disable no-console */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import { Layout, Row, Spin } from 'antd';

import { appName, identityProvider } from '../../config';

class AuthenticationCallback extends Component {
  // eslint-disable-next-line react/static-property-placement
  static propTypes = {
    history: PropTypes.instanceOf(Object).isRequired,
  };

  // eslint-disable-next-line react/no-deprecated
  componentWillMount() {
    const { history } = this.props;
    const params = this.getParams(window.location.href);
    axios
      .get(`${identityProvider}/connect/userinfo`, {
        headers: { Authorization: `Bearer ${params.access_token}` },
      })
      .then((response) => {
        sessionStorage.setItem(`${appName}_id_token`, params.id_token);
        sessionStorage.setItem(`${appName}_access_token`, params.access_token);
        return response.data;
      })
      .then((data) => {
        axios
          .get(`${identityProvider}/api/Connect/GetApplicationUserByName/${data.name}`)
          .then((response) => {
            const user = response.data.resultSet;
            if (user.isActive) {
              sessionStorage.setItem(`${appName}_user_id`, user.userName);
              sessionStorage.setItem(`${appName}_full_name`, user.fullName);
            }
            return user;
          })

          .then((user) => {
            axios
              .post(`${identityProvider}/api/Connect/GetUserMenusByClientId`, {
                UserName: user.userName,
                ClientId: process.env.REACT_APP_CLIENT_ID,
              })
              .then((response) => {
                const { isSuccess, resultSet } = response.data;
                if (isSuccess) {
                  if (resultSet.length) {
                    sessionStorage.setItem(`${appName}_menus`, JSON.stringify(resultSet));
                  } else {
                    console.warn('Kullanıcının yektili olduğu menü yok!');
                    if (process.env.NODE_ENV !== 'development') {
                      sessionStorage.setItem(`${appName}_menus`, '[]');
                    }
                  }
                }

                axios
                  .get(`${identityProvider}/api/Connect/GetUserApplications/${user.userName}`)
                  .then((resp) => {
                    sessionStorage.setItem('userApplications', JSON.stringify(resp.data.resultSet));
                    history.replace(decodeURIComponent(params.state));
                  })
                  .catch(() => {
                    console.error('Kullanıcının yetkili olduğu uygulamalar alınamadı!');
                    history.replace(decodeURIComponent(params.state));
                  });
              });
          });
      });
  }

  getParams = (url) => {
    const regex = /[?&]([^=#]+)=([^&#]*)/g;
    let match = null;
    const params = {};
    /* eslint-disable-next-line */
    while ((match = regex.exec(url))) params[match[1]] = match[2];
    return params;
  };

  render() {
    return (
      <Layout.Content>
        <Row type="flex" justify="space-around" align="middle" style={{ height: '100vh' }}>
          <Spin size="large" />
        </Row>
      </Layout.Content>
    );
  }
}

export default AuthenticationCallback;

import React from 'react';

import { appName, authentication } from '../../config';

const Logout = () => {
  const { logoutEndpoint, logoutRedirectUri } = authentication;
  const idToken = sessionStorage.getItem(`${appName}_id_token`);
  sessionStorage.clear();
  window.location = `${logoutEndpoint}?id_token_hint=${idToken}&post_logout_redirect_uri=${logoutRedirectUri}`;
  return <></>;
};

export default Logout;

import React from 'react';

import { authentication } from '../../config';
import { generateNonce, queryString } from '../../components/Helper';

const getAuthUrl = (returnUrl) => `${authentication.authorizationEndpoint}?response_type=id_token%20token
&client_id=${authentication.clientId}&scope=${authentication.scopes}&redirect_uri=${authentication.redirectUri}
&nonce=${generateNonce()}&state=${encodeURI(returnUrl)}`;

const Login = () => {
  const redirect = queryString('redirect');
  window.location = getAuthUrl(redirect === null ? '/' : redirect);
  return <></>;
};

export default Login;

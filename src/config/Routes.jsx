import PropTypes from 'prop-types';
import React, { lazy, Suspense } from 'react';
import { Switch } from 'react-router-dom';

import Loading from '../components/Loading';
// import AuthenticatedRoute from '../components/Routes/AuthenticatedRoute';
import UnauthenticatedRoute from '../components/Routes/UnauthenticatedRoute';
import Callback from '../pages/authantication/Callback';
import Login from '../pages/authantication/Login';
import Logout from '../pages/authantication/Logout';

const loadable = (WrappedComponent) => (
  <Suspense fallback={Loading}>
    <WrappedComponent />
  </Suspense>
);

const Basic = loadable(lazy(() => import('../pages/main')));
const NotFound = loadable(lazy(() => import('../pages/errors/NotFound')));

const Routes = ({ childProps }) => (
  <Switch>
    <UnauthenticatedRoute path="/" component={() => Basic} props={childProps} exact />
    <UnauthenticatedRoute path="/Basic" component={() => Basic} props={childProps} exact />
    <UnauthenticatedRoute path="/logout" component={Logout} layout="empty" exact />
    <UnauthenticatedRoute path="/login" component={Login} layout="empty" exact />
    <UnauthenticatedRoute path="/authentication/callback" component={Callback} layout="empty" />
    <UnauthenticatedRoute component={() => NotFound} />
  </Switch>
);

Routes.defaultProps = {
  childProps: {},
};

Routes.propTypes = {
  childProps: PropTypes.instanceOf(Object),
};

export default Routes;

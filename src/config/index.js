export const apiUrl = process.env.REACT_APP_API_URL;
export const appName = process.env.REACT_APP_APP_NAME;
export const identityProvider = process.env.REACT_APP_IDENTITY_PROVIDER;

export const authentication = {
  scopes: process.env.REACT_APP_SCOPES,
  clientId: process.env.REACT_APP_CLIENT_ID,
  redirectUri: process.env.REACT_APP_REDIRECT_URI,
  logoutEndpoint: `${identityProvider}/connect/endsession`,
  logoutRedirectUri: process.env.REACT_APP_LOGOUT_REDIRECT_URI,
  authorizationEndpoint: `${identityProvider}/connect/authorize`,
};
